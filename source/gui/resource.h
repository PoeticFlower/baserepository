//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Win32MainApp.rc
//
#ifndef IDC_STATIC
#define IDC_STATIC                     -1
#endif
#define IDC_MYICON                      2
#define IDD_ABOUTBOX                    100
#define IDD_MAINAPP_DIALOG              101
#define IDS_MAINAPP_TITLE               102
#define IDM_ABOUT                       103
#define IDM_EXIT                        104
#define IDI_MAINAPP_NORMAL              105
#define IDI_MAINAPP_SMALL               106
#define IDC_WIN32MAINAPP                107
#define IDR_MAINFRAME                   128

// Next default values for new objects
//
#ifdef  APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     130
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           108
#endif
#endif
