file(GLOB MFCAPP_HEADER_FILES
    LIST_DIRECTORIES false
    resource.h
    MFCMainDlg.h
    MFCMainApp.h
    )
file(GLOB MFCAPP_SOURCE_FILES
    LIST_DIRECTORIES false
    MFCMainDlg.cpp
    MFCMainApp.cpp
    )
file(GLOB MFCAPP_RESOURCE_FILES
    LIST_DIRECTORIES false
    MFCMainApp.rc
    res/MFCMainApp.rc2
    )

include_directories(..)

if(WIN32)
    if(USE_STATIC_CRT_LIBRARY)
        set(CMAKE_MFC_FLAG 1)
    else(USE_STATIC_CRT_LIBRARY)
        set(CMAKE_MFC_FLAG 2)
    endif(USE_STATIC_CRT_LIBRARY)

    add_executable(MFCApp WIN32
        ${GUICOMMON_SOURCE_FILES}
        ${WIN32COMMON_HEADER_FILES}
        ${WIN32COMMON_SOURCE_FILES}
        ${WIN32COMMON_RESOURCE_FILES}
        ${MFCAPP_HEADER_FILES}
        ${MFCAPP_SOURCE_FILES}
        ${MFCAPP_RESOURCE_FILES}
        )

    if(USE_STATIC_CRT_LIBRARY)
    else(USE_STATIC_CRT_LIBRARY)
        target_compile_definitions(MFCApp
            PUBLIC -D_AFXDLL=1
            )
    endif(USE_STATIC_CRT_LIBRARY)

    target_compile_definitions(MFCApp
        PUBLIC -DGUI_APPLICATION_USING_MFC=1
        PUBLIC -D_UNICODE
        PUBLIC -DUNICODE
        )
endif(WIN32)
